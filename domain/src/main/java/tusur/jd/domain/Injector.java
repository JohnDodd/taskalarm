package tusur.jd.domain;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import tusur.jd.domain.interactors.AlarmsInteractor;
import tusur.jd.domain.interactors.NotesInteractor;
import tusur.jd.domain.interactors.RecordsInteractor;
import tusur.jd.domain.repository.IRepositoryProvider;

public class Injector {

	private final NotesInteractor mNotesInteractor;
	private final RecordsInteractor mRecordsInteractor;
	private final AlarmsInteractor mAlarmsInteractor;

	public Injector(final IRepositoryProvider repositoryProvider) {
		final ExecutorService executor = Executors.newCachedThreadPool();
		final Scheduler scheduler = Schedulers.from(executor);
		mNotesInteractor = new NotesInteractor(repositoryProvider.getNotesRepository(), scheduler);
		mRecordsInteractor = new RecordsInteractor(repositoryProvider.getRecordsRepository(), scheduler);
		mAlarmsInteractor = new AlarmsInteractor(repositoryProvider.getAlarmsRepository(), scheduler);
	}

	public NotesInteractor getNotesInteractor() {
		return mNotesInteractor;
	}

	public RecordsInteractor getRecordsInteractor() {
		return mRecordsInteractor;
	}

	public AlarmsInteractor getAlarmsInteractor() {
		return mAlarmsInteractor;
	}
}
