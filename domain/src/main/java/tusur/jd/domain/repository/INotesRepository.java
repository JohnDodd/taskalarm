package tusur.jd.domain.repository;

import java.util.List;

import io.reactivex.Observable;
import tusur.jd.domain.models.Note;

public interface INotesRepository {

	long insert(String name);
	int deleteAll();
	int delete(long id);
	int update(long id, String name);
	Observable<List<Note>> all();
}
