package tusur.jd.domain.repository;

import java.util.List;

import io.reactivex.Observable;
import tusur.jd.domain.models.Record;

public interface IRecordsRepository {

	Observable<List<Record>> allByNoteId(long noteId);
	void update(String text, long id);
	void insert(String text, long noteId);
}
