package tusur.jd.domain.repository;

public interface IRepositoryProvider {

	INotesRepository getNotesRepository();
	IRecordsRepository getRecordsRepository();
	IAlarmsRepository getAlarmsRepository();
}
