package tusur.jd.domain.repository;

import java.util.List;

import io.reactivex.Observable;
import tusur.jd.domain.models.Alarm;

public interface IAlarmsRepository {

	long addSingleTime(int hours, int minutes, long noteId);
	long updateSingleTime(final long alarmId, int hours, int minutes, long noteId);
	int delete(int id);
	int deleteAll();
	Observable<List<Alarm>> all();
}
