package tusur.jd.domain.models;

public class Record {

	private final long mId;
	private final long mNoteId;
	private final String mText;

	public Record(final long id, final long noteId, final String text) {
		mId = id;
		mNoteId = noteId;
		mText = text;
	}

	public long getId() {
		return mId;
	}

	public long getNoteId() {
		return mNoteId;
	}

	public String getText() {
		return mText;
	}
}
