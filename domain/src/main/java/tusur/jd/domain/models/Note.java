package tusur.jd.domain.models;


public class Note {

	private final long mId;
	private final String mName;
	private final long mTimeMillis;

	public Note(final long id, final String name, final long timeMillis) {
		mId = id;
		mName = name;
		mTimeMillis = timeMillis;
	}

	public long getId() {
		return mId;
	}

	public String getName() {
		return mName;
	}

	public long getTimeMillis() {
		return mTimeMillis;
	}
}
