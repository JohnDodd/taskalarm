package tusur.jd.domain.models;

public class Alarm {

	private final long mId;
	private final int mHours;
	private final int mMinutes;
	private final long mNoteId;

	public Alarm(final long id, final int hours, final int minutes, final long noteId) {
		mId = id;
		mHours = hours;
		mMinutes = minutes;
		mNoteId = noteId;
	}

	public long getId() {
		return mId;
	}

	public int getHours() {
		return mHours;
	}

	public int getMinutes() {
		return mMinutes;
	}

	public long getNoteId() {
		return mNoteId;
	}
}
