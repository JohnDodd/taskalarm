package tusur.jd.domain.interactors;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.subjects.BehaviorSubject;
import tusur.jd.domain.models.Note;
import tusur.jd.domain.repository.INotesRepository;

public class NotesInteractor {

	private INotesRepository mRepository;
	private final Scheduler mScheduler;

	private final BehaviorSubject<Optional<List<Note>>> mNotesSubject = BehaviorSubject.createDefault(Optional.empty());

	public NotesInteractor(final INotesRepository repository, final Scheduler scheduler) {
		mRepository = repository;
		mScheduler = scheduler;
	}

	public void delete(final long id) {
		applyCache(() -> mRepository.delete(id));
	}

	public void deleteAll() {
		applyCache(mRepository::deleteAll);
	}

	public ConnectableObservable<Long> insert(final String name) {
		final ConnectableObservable<Long> publish = Observable.fromCallable(() -> mRepository.insert(name))
				.flatMap(it -> getNotesObservable().flatMap(a -> Observable.just(it)))
				.subscribeOn(mScheduler)
				.publish();
		publish.connect();
		return publish;
	}

	public void update(final long noteId, final String name) {
		applyCache(() -> mRepository.update(noteId, name));
	}

	public Observable<Note> getNoteObservable(final long id) {
		return getNotesObservable().map(it -> Stream.of(it).filter(note -> note.getId() == id).findFirst())
				.filter(Optional::isPresent)
				.map(Optional::get);
	}

	public Observable<List<Note>> getNotesObservable() {
		return mNotesSubject.withLatestFrom(getNoteFromStorage(), (a, b) -> a)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.subscribeOn(mScheduler);
	}

	private Observable<List<Note>> getNoteFromStorage() {
		return mRepository.all().doOnNext(it -> mNotesSubject.onNext(Optional.ofNullable(it)));
	}

	private <T> void applyCache(final Callable<T> callable) {
		Observable.fromCallable(callable)
				.flatMap(it -> getNotesObservable())
				.subscribeOn(mScheduler)
				.publish()
				.connect();
	}
}
