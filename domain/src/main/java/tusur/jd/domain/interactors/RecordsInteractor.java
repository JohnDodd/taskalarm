package tusur.jd.domain.interactors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.subjects.BehaviorSubject;
import tusur.jd.domain.models.Record;
import tusur.jd.domain.repository.IRecordsRepository;

public class RecordsInteractor {

	private final IRecordsRepository mRepository;
	private final Scheduler mScheduler;

	private final BehaviorSubject<Map<Long, List<Record>>> mRecordSubject = BehaviorSubject.createDefault(new HashMap<>());

	public RecordsInteractor(final IRecordsRepository repository, final Scheduler scheduler) {
		mRepository = repository;
		mScheduler = scheduler;
	}

	public Observable<List<Record>> getRecordsObservable(final long noteId) {
		return mRecordSubject.hide().withLatestFrom(getRecordsFromStorage(noteId), (a, b) -> b).subscribeOn(mScheduler);
	}

	private Observable<List<Record>> getRecordsFromStorage(final long noteId) {
		return mRepository.allByNoteId(noteId).doOnNext(it -> {
			final HashMap<Long, List<Record>> map = new HashMap<>(mRecordSubject.getValue());
			map.put(noteId, it);
			mRecordSubject.onNext(map);
		});
	}

	public void add(final String text, final long noteId) {
		Completable.fromAction(() -> mRepository.insert(text, noteId))
				.toObservable()
				.subscribeOn(mScheduler)
				.doOnComplete(() -> getRecordsObservable(noteId))
				.publish()
				.connect();
	}

	public void update(final String text, final long id, final long noteId) {
		Completable.fromAction(() -> mRepository.update(text, id))
				.toObservable()
				.subscribeOn(mScheduler)
				.doOnComplete(() -> getRecordsObservable(noteId))
				.publish()
				.connect();

	}
}
