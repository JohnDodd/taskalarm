package tusur.jd.domain.interactors;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.subjects.BehaviorSubject;
import tusur.jd.domain.models.Alarm;
import tusur.jd.domain.repository.IAlarmsRepository;

public class AlarmsInteractor {

	private final IAlarmsRepository mRepository;
	private final Scheduler mScheduler;
	private final BehaviorSubject<List<Alarm>> mAlarmsSubject = BehaviorSubject.createDefault(new ArrayList<>());

	public AlarmsInteractor(final IAlarmsRepository repository, final Scheduler scheduler) {
		mRepository = repository;
		mScheduler = scheduler;
	}

	public void addSingleTimeAlarm(final int hours, final int minutes) {
		addSingleTimeAlarm(hours, minutes, -1);
	}

	public void addSingleTimeAlarm(final int hours, final int minutes, final long noteId) {
		applyCache(() -> mRepository.addSingleTime(hours, minutes, noteId));
	}

	public void updateSingleTimeAlarm(final long alarmId, final int hours, final int minutes, final long noteId) {
		applyCache(() -> mRepository.updateSingleTime(alarmId, hours, minutes, noteId));
	}

	public void delete(final int id) {
		applyCache(() -> mRepository.delete(id));
	}

	public void deleteAll() {
		applyCache(mRepository::deleteAll);
	}

	public Observable<Optional<Alarm>> getAlarmOptionalObservable(final long id) {
		return getAlarmsObservable().map(alarms -> Stream.of(alarms).filter(alarm -> alarm.getId() == id).findFirst());
	}

	public Observable<Alarm> getAlarmObservable(final long id) {
		return getAlarmOptionalObservable(id)
				.filter(Optional::isPresent)
				.map(Optional::get);
	}

	public Observable<List<Alarm>> getAlarmsObservable() {
		return mAlarmsSubject.withLatestFrom(getAlarmsFromStorage(), (a, b) -> a).subscribeOn(mScheduler);
	}

	private Observable<List<Alarm>> getAlarmsFromStorage() {
		return mRepository.all().doOnNext(mAlarmsSubject::onNext);
	}

	private <T> void applyCache(final Callable<T> callable) {
		Observable.fromCallable(callable)
				.flatMap(it -> getAlarmsObservable())
				.subscribeOn(mScheduler)
				.publish()
				.connect();
	}

}
