package tusur.jd.data;

import org.junit.Test;

import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

	@Test
	public void addition_isCorrect() {
		assertEquals(4, 2 + 2);
	}

	@Test
	public void testAutoSaveSubject() throws InterruptedException {
		final BehaviorSubject<String> subject = BehaviorSubject.createDefault("");
		final Random random = new Random();
		final Observable<String> storageObservable = Observable
				.fromCallable(() -> "not empty " + random.nextInt(100))
				.doOnNext(subject::onNext);
		final Observable<String> hide = subject.hide().withLatestFrom(storageObservable, (a, b) -> a);

		hide.map(it -> "onNext: " + it).subscribe(System.out::println);
		hide.map(it -> "onNext 2: " + it).subscribe(System.out::println);

		final Observable<Object> completable = Observable
				.fromCallable(() -> "callable value");
		completable.subscribe(it -> System.out.println("onComplete"));

		System.out.println("subject value: " + subject.getValue());
		Thread.sleep(500);
	}
}