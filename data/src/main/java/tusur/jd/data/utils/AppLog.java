package tusur.jd.data.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import tusur.jd.data.BuildConfig;

public class AppLog {

	public static void d(@NonNull final String tag, @NonNull final String message) {
		if (BuildConfig.DEBUG) {
			Log.d(tag, message);
		}
	}

	public static void e(@NonNull final String tag, @NonNull final String message, @NonNull final Throwable throwable) {
		if (BuildConfig.DEBUG) {
			Log.e(tag, message, throwable);
		}
	}
}
