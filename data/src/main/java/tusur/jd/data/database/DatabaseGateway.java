package tusur.jd.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.annimon.stream.Stream;

import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_ID;
import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_NAME;
import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_TIME;
import static tusur.jd.data.database.DatabaseGateway.KeyRecord.KEY_FOREIGN_NOTE_ID;
import static tusur.jd.data.database.DatabaseGateway.KeyRecord.KEY_TEXT;

public class DatabaseGateway extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 5;
	public static final String DATABASE_NAME = "database";
	public static final String TABLE_NOTES = "TABLE_NOTES";
	public static final String TABLE_RECORDS = "TABLE_RECORDS";
	public static final String TABLE_ALARMS = "TABLE_ALARMS";
	private static final String TAG = DatabaseGateway.class.getSimpleName();

	public DatabaseGateway(final Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(final SQLiteDatabase sqLiteDatabase) {
		sqLiteDatabase.execSQL("create table " + TABLE_NOTES
		                       + "("
		                       + KEY_ID + " integer primary key autoincrement,"
		                       + KEY_NAME + " text,"
		                       + KEY_TIME + " timestamp default current_timestamp"
		                       + ")");
		sqLiteDatabase.execSQL("create table " + TABLE_RECORDS
		                       + "("
		                       + KeyRecord.KEY_ID + " integer primary key autoincrement,"
		                       + KEY_TEXT + " text,"
		                       + KEY_FOREIGN_NOTE_ID + " integer, "
		                       + "FOREIGN KEY " +
		                       "(" + KeyAlarm.KEY_FOREIGN_NOTE_ID + ") REFERENCES " + TABLE_NOTES + "(" + KeyNote.KEY_ID + ")"
		                       + ")");
		sqLiteDatabase.execSQL("create table " + TABLE_ALARMS
		                       + "("
		                       + KeyAlarm.KEY_ID + " integer primary key autoincrement,"
		                       + KeyAlarm.KEY_HOURS + " integer,"
		                       + KeyAlarm.KEY_MINUTES + " integer,"
		                       + KeyAlarm.KEY_FOREIGN_NOTE_ID + " integer, "
		                       + "FOREIGN KEY " +
		                       "(" + KeyAlarm.KEY_FOREIGN_NOTE_ID + ") REFERENCES " + TABLE_NOTES + "(" + KeyNote.KEY_ID + ")"
		                       + ")");
	}

	@Override
	public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int i, final int i1) {
		sqLiteDatabase.execSQL("drop table if exists " + TABLE_NOTES);
		sqLiteDatabase.execSQL("drop table if exists " + TABLE_RECORDS);
		sqLiteDatabase.execSQL("drop table if exists " + TABLE_ALARMS);
		onCreate(sqLiteDatabase);
	}

	private <T> T readable(@NonNull final Listener<T> listener) {
		return listener.act(getReadableDatabase());
	}

	private <T> T writable(@NonNull final Listener<T> listener) {
		return listener.act(getWritableDatabase());
	}

	@NonNull
	public QueryBuilder queryBuilder(@NonNull final String tableName) {
		return new QueryBuilder(tableName);
	}

	public static class KeyNote {

		public static final String KEY_ID = "_id";
		public static final String KEY_NAME = "name";
		public static final String KEY_TIME = "time";
	}

	public static class KeyRecord {

		public static final String KEY_ID = "_id";
		public static final String KEY_TEXT = "text";
		public static final String KEY_FOREIGN_NOTE_ID = "note_id";
	}

	public static class KeyAlarm {

		public static final String KEY_ID = "_id";
		public static final String KEY_HOURS = "hours";
		public static final String KEY_MINUTES = "minutes";
		public static final String KEY_FOREIGN_NOTE_ID = "note_id";
	}

	public class QueryBuilder {

		@NonNull
		private final ContentValues mContentValues = new ContentValues();
		@NonNull
		private final String mTableName;

		private String[] mColumns;
		private String mSelection;
		private String[] mSelectionArgs;
		private String mGroupBy;
		private String mHaving;
		private String mOrderBy;

		private QueryBuilder(@NonNull final String tableName) {
			mTableName = tableName;
		}

		public QueryBuilder put(final String key, final String value) {
			mContentValues.put(key, value);
			return this;
		}

		public QueryBuilder put(final String key, final long value) {
			mContentValues.put(key, value);
			return this;
		}

		public int update() {
			return writable(database -> database.update(
					mTableName,
					mContentValues,
					mSelection,
					Stream.ofNullable(mSelectionArgs).map(Object::toString).toArray(String[]::new)
			));
		}

		public int update(@NonNull final String whereClause, @NonNull final Object... whereArgs) {
			return writable(database -> database.update(
					mTableName,
					mContentValues,
					whereClause,
					Stream.ofNullable(whereArgs).map(Object::toString).toArray(String[]::new)
			));
		}

		public int delete() {
			return writable(database -> database.delete(
					mTableName,
					mSelection,
					Stream.ofNullable(mSelectionArgs).map(Object::toString).toArray(String[]::new)
			));
		}

		public int delete(@Nullable final String whereClause, @Nullable final Object... whereArgs) {
			return writable(database -> database.delete(
					mTableName,
					whereClause,
					Stream.ofNullable(whereArgs).map(Object::toString).toArray(String[]::new)
			));
		}

		public long insert() {
			return writable(database -> database.insert(mTableName, null, mContentValues));
		}

		public QueryBuilder columns(final Object... columns) {
			mColumns = Stream.of(columns).map(Object::toString).toArray(String[]::new);
			return this;
		}

		public QueryBuilder selection(final String selection) {
			mSelection = selection;
			return this;
		}

		public QueryBuilder selection(final String selection, final Object... selectionArgs) {
			mSelection = selection;
			selectionArgs(selectionArgs);
			return this;
		}

		public QueryBuilder selectionArgs(final Object... selectionArgs) {
			mSelectionArgs = Stream.ofNullable(selectionArgs).map(Object::toString).toArray(String[]::new);
			return this;
		}

		public QueryBuilder groupBy(final String groupBy) {
			mGroupBy = groupBy;
			return this;
		}

		public QueryBuilder having(final String having) {
			mHaving = having;
			return this;
		}

		public QueryBuilder orderBy(final String orderBy) {
			mOrderBy = orderBy;
			return this;
		}

		public Cursor get() {
			return readable(database -> database.query(mTableName, mColumns, mSelection, mSelectionArgs, mGroupBy, mHaving, mOrderBy));
		}

		public Cursor rawQuery(final String sql, final Object... args) {
			return readable(database -> database.rawQuery(sql, Stream.ofNullable(args).map(Object::toString).toArray(String[]::new)));
		}
	}

	public interface Listener <T> {

		T act(@NonNull SQLiteDatabase database);
	}
}
