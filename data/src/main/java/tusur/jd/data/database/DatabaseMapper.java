package tusur.jd.data.database;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import tusur.jd.data.utils.AppLog;
import tusur.jd.domain.models.Alarm;
import tusur.jd.domain.models.Note;
import tusur.jd.domain.models.Record;

import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_ID;
import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_NAME;
import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_TIME;

public class DatabaseMapper {

	private static final String TAG = DatabaseMapper.class.getSimpleName();
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private SimpleDateFormat mSimpleDateFormat;

	public DatabaseMapper() {
		mSimpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
	}

	@NonNull
	public List<Note> mapNotes(@NonNull final Cursor cursor) {
		final List<Note> notes = new ArrayList<>();
		final int idIndex = cursor.getColumnIndex(KEY_ID);
		final int nameIndex = cursor.getColumnIndex(KEY_NAME);
		final int timeIndex = cursor.getColumnIndex(KEY_TIME);
		forEach(cursor, () -> {
			long time = 0;
			try {
				time = mSimpleDateFormat.parse(cursor.getString(timeIndex)).getTime();
			} catch (final ParseException e) {
				AppLog.e(TAG, "mapNotes: ", e);
			}

			notes.add(new Note(
					cursor.getLong(idIndex),
					cursor.getString(nameIndex),
					time
			));
		});
		return notes;
	}

	@NonNull
	public List<Record> mapRecords(@NonNull final Cursor cursor) {
		final List<Record> records = new ArrayList<>();
		final int idIndex = cursor.getColumnIndex(DatabaseGateway.KeyRecord.KEY_ID);
		final int foreignNoteIdIndex = cursor.getColumnIndex(DatabaseGateway.KeyRecord.KEY_FOREIGN_NOTE_ID);
		final int textIndex = cursor.getColumnIndex(DatabaseGateway.KeyRecord.KEY_TEXT);
		forEach(cursor, () ->
				records.add(new Record(
						cursor.getLong(idIndex),
						cursor.getLong(foreignNoteIdIndex),
						cursor.getString(textIndex)
				))
		);
		return records;
	}

	public List<Alarm> mapAlarms(final Cursor cursor) {
		final List<Alarm> alarms = new ArrayList<>();
		final int idIndex = cursor.getColumnIndex(DatabaseGateway.KeyAlarm.KEY_ID);
		final int hoursIndex = cursor.getColumnIndex(DatabaseGateway.KeyAlarm.KEY_HOURS);
		final int minutesIndex = cursor.getColumnIndex(DatabaseGateway.KeyAlarm.KEY_MINUTES);
		final int noteIdIndex = cursor.getColumnIndex(DatabaseGateway.KeyAlarm.KEY_FOREIGN_NOTE_ID);
		forEach(cursor, () ->
				alarms.add(new Alarm(
						cursor.getLong(idIndex),
						cursor.getInt(hoursIndex),
						cursor.getInt(minutesIndex),
						cursor.getLong(noteIdIndex)
				)));
		return alarms;
	}

	private void forEach(@NonNull final Cursor cursor, @NonNull final Action action) {
		if (cursor.moveToFirst()) {
			do {
				action.act();
			} while (cursor.moveToNext());
		}
		cursor.close();
	}

	private interface Action {

		void act();
	}
}
