package tusur.jd.data.repository;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.AlarmManagerCompat;

import java.util.Calendar;
import java.util.List;

import io.reactivex.Observable;
import tusur.jd.data.database.DatabaseGateway;
import tusur.jd.data.database.DatabaseMapper;
import tusur.jd.domain.models.Alarm;
import tusur.jd.domain.repository.IAlarmsRepository;

import static tusur.jd.data.database.DatabaseGateway.KeyAlarm.KEY_FOREIGN_NOTE_ID;
import static tusur.jd.data.database.DatabaseGateway.KeyAlarm.KEY_HOURS;
import static tusur.jd.data.database.DatabaseGateway.KeyAlarm.KEY_ID;
import static tusur.jd.data.database.DatabaseGateway.KeyAlarm.KEY_MINUTES;
import static tusur.jd.data.database.DatabaseGateway.TABLE_ALARMS;

public class AlarmsRepository implements IAlarmsRepository {

	@NonNull
	private final DatabaseGateway mDatabaseGateway;
	@NonNull
	private final DatabaseMapper mMapper;
	@NonNull
	private final Context mContext;
	@NonNull
	private final Class<? extends BroadcastReceiver> mAlarmReceiverClass;
	private final AlarmManager mAlarmManager;
	public static final String KEY_FROM_ALARM = "KEY_FROM_ALARM";

	public static final String PATH_VEHICLE = "reminder-path";
	public static final String CONTENT_AUTHORITY = "com.delaroystudios.alarmreminder";
	public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
	public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_VEHICLE);

	public AlarmsRepository(@NonNull final DatabaseGateway databaseGateway,
	                        @NonNull final DatabaseMapper mapper,
	                        @NonNull final Context context,
	                        @NonNull final Class<? extends BroadcastReceiver> alarmReceiverClass) {
		mDatabaseGateway = databaseGateway;
		mMapper = mapper;
		mContext = context;
		mAlarmReceiverClass = alarmReceiverClass;
		mAlarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
	}

	@Override
	public long addSingleTime(final int hours, final int minutes, final long noteId) {
		final long alarmId = mDatabaseGateway.queryBuilder(TABLE_ALARMS)
				.put(KEY_HOURS, hours)
				.put(KEY_MINUTES, minutes)
				.put(KEY_FOREIGN_NOTE_ID, noteId)
				.insert();
		scheduleSingleTimeAlarm(hours, minutes, alarmId);
		return alarmId;
	}

	@Override
	public long updateSingleTime(final long alarmId, final int hours, final int minutes, final long noteId) {
		final long result = mDatabaseGateway.queryBuilder(TABLE_ALARMS)
				.put(KEY_HOURS, hours)
				.put(KEY_MINUTES, minutes)
				.put(KEY_FOREIGN_NOTE_ID, noteId)
				.selection(KEY_ID + " = ?", alarmId)
				.update();
		updateScheduledSingleTimeAlarm(alarmId, hours, minutes);
		return result;
	}

	@Override
	public int delete(final int id) {
		return mDatabaseGateway.queryBuilder(TABLE_ALARMS)
				.selection(KEY_ID + " = ?", id)
				.delete();
	}

	@Override
	public int deleteAll() {
		return mDatabaseGateway.queryBuilder(TABLE_ALARMS).delete();
	}

	@Override
	public Observable<List<Alarm>> all() {
		return Observable.fromCallable(() -> mMapper.mapAlarms(
				mDatabaseGateway.queryBuilder(TABLE_ALARMS)
						.orderBy(KEY_ID + " asc")
						.get()));
	}


	private void cancelAlarm(final long alarmId) {
		final Intent intent = new Intent(mContext, mAlarmReceiverClass);
		intent.setData(getAlarmUri(alarmId));
		final PendingIntent alarmIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		mAlarmManager.cancel(alarmIntent);
	}

	private void updateScheduledSingleTimeAlarm(final long alarmId, final int hours, final int minutes) {
		cancelAlarm(alarmId);
		scheduleSingleTimeAlarm(hours, minutes, alarmId);
	}

	private void scheduleSingleTimeAlarm(final int hours, final int minutes, final long alarmId) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, hours);
		calendar.set(Calendar.MINUTE, minutes);

		final Intent intent = new Intent(mContext, mAlarmReceiverClass);
		intent.putExtra(DatabaseGateway.KeyAlarm.KEY_ID, alarmId);
		intent.putExtra(KEY_FROM_ALARM, true);

		intent.setData(getAlarmUri(alarmId));

		final PendingIntent alarmIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManagerCompat.setExact(mAlarmManager, AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
	}

	@NonNull
	private Uri getAlarmUri(final long alarmId) {
		return ContentUris.withAppendedId(CONTENT_URI, alarmId);
	}

}
