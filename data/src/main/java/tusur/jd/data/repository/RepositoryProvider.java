package tusur.jd.data.repository;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.support.annotation.NonNull;

import tusur.jd.data.database.DatabaseGateway;
import tusur.jd.data.database.DatabaseMapper;
import tusur.jd.domain.repository.IAlarmsRepository;
import tusur.jd.domain.repository.INotesRepository;
import tusur.jd.domain.repository.IRecordsRepository;
import tusur.jd.domain.repository.IRepositoryProvider;

public class RepositoryProvider implements IRepositoryProvider {

	private Context mContext;
	private final NotesRepository mNotesRepository;
	private final RecordsRepository mRecordsRepository;
	private final AlarmsRepository mAlarmsRepository;

	public RepositoryProvider(@NonNull final Context context, @NonNull final Class<? extends BroadcastReceiver> alarmReceiverClass) {
		mContext = context;
		final DatabaseGateway databaseGateway = new DatabaseGateway(context);
		final DatabaseMapper databaseMapper = new DatabaseMapper();
		mNotesRepository = new NotesRepository(databaseGateway, databaseMapper);
		mRecordsRepository = new RecordsRepository(databaseGateway, databaseMapper);
		mAlarmsRepository = new AlarmsRepository(databaseGateway, databaseMapper, context, alarmReceiverClass);
	}

	@NonNull
	@Override
	public INotesRepository getNotesRepository() {
		return mNotesRepository;
	}

	@NonNull
	@Override
	public IRecordsRepository getRecordsRepository() {
		return mRecordsRepository;
	}

	@NonNull
	@Override
	public IAlarmsRepository getAlarmsRepository() {
		return mAlarmsRepository;
	}
}
