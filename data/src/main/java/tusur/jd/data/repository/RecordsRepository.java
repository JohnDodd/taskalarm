package tusur.jd.data.repository;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Observable;
import tusur.jd.data.database.DatabaseGateway;
import tusur.jd.data.database.DatabaseMapper;
import tusur.jd.domain.models.Record;
import tusur.jd.domain.repository.IRecordsRepository;

import static tusur.jd.data.database.DatabaseGateway.KeyRecord.KEY_FOREIGN_NOTE_ID;
import static tusur.jd.data.database.DatabaseGateway.KeyRecord.KEY_ID;
import static tusur.jd.data.database.DatabaseGateway.KeyRecord.KEY_TEXT;
import static tusur.jd.data.database.DatabaseGateway.TABLE_RECORDS;

public class RecordsRepository implements IRecordsRepository {

	@NonNull
	private final DatabaseGateway mDatabaseGateway;
	@NonNull
	private final DatabaseMapper mDatabaseMapper;

	public RecordsRepository(@NonNull final DatabaseGateway databaseGateway, @NonNull final DatabaseMapper databaseMapper) {
		mDatabaseGateway = databaseGateway;
		mDatabaseMapper = databaseMapper;
	}

	@Override
	public Observable<List<Record>> allByNoteId(final long noteId) {
		return Observable.fromCallable(() -> mDatabaseMapper.mapRecords(
				mDatabaseGateway.queryBuilder(TABLE_RECORDS)
						.selection(KEY_FOREIGN_NOTE_ID + " = ?", noteId)
						.get()));
	}

	@Override
	public void update(final String text, final long id) {
		mDatabaseGateway.queryBuilder(TABLE_RECORDS)
				.put(KEY_TEXT, text)
				.update(KEY_ID + " = ?", id);
	}

	@Override
	public void insert(final String text, final long noteId) {
		mDatabaseGateway.queryBuilder(TABLE_RECORDS)
				.put(KEY_TEXT, text)
				.put(KEY_FOREIGN_NOTE_ID, noteId)
				.insert();
	}
}
