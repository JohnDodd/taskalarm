package tusur.jd.data.repository;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Observable;
import tusur.jd.data.database.DatabaseGateway;
import tusur.jd.data.database.DatabaseMapper;
import tusur.jd.domain.models.Note;
import tusur.jd.domain.repository.INotesRepository;

import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_ID;
import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_NAME;
import static tusur.jd.data.database.DatabaseGateway.KeyNote.KEY_TIME;
import static tusur.jd.data.database.DatabaseGateway.TABLE_NOTES;

public class NotesRepository implements INotesRepository {

	@NonNull
	private final DatabaseGateway mDatabaseGateway;
	@NonNull
	private final DatabaseMapper mDatabaseMapper;

	public NotesRepository(@NonNull final DatabaseGateway databaseGateway, @NonNull final DatabaseMapper databaseMapper) {
		mDatabaseGateway = databaseGateway;
		mDatabaseMapper = databaseMapper;
	}

	@Override
	public long insert(@NonNull final String name) {
		return mDatabaseGateway.queryBuilder(TABLE_NOTES)
				.put(KEY_NAME, name)
				.insert();
	}

	@Override
	public int deleteAll() {
		return mDatabaseGateway.queryBuilder(TABLE_NOTES).delete();
	}

	@Override
	public int delete(final long id) {
		return mDatabaseGateway.queryBuilder(TABLE_NOTES)
				.delete(KEY_ID + " = ?", id);
	}

	@Override
	public int update(final long id, final String name) {
		return mDatabaseGateway.queryBuilder(TABLE_NOTES)
				.put(KEY_NAME, name)
				.update(KEY_ID + " = ?", id);
	}

	@Override
	public Observable<List<Note>> all() {
		return Observable.fromCallable(() -> mDatabaseMapper.mapNotes(mDatabaseGateway.queryBuilder(TABLE_NOTES)
				.rawQuery("select " + KEY_ID + ", " + KEY_NAME + ", " + KEY_TIME + " "
				          + "from " + TABLE_NOTES + " "
				          + "order by " + KEY_ID + " desc")));
	}
}
