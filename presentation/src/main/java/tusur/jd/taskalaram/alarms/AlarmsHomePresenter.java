package tusur.jd.taskalaram.alarms;

import android.support.annotation.NonNull;

import com.annimon.stream.Collectors;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import tusur.jd.domain.interactors.AlarmsInteractor;
import tusur.jd.domain.interactors.NotesInteractor;
import tusur.jd.domain.models.Alarm;
import tusur.jd.domain.models.Note;
import tusur.jd.taskalaram.app.UiRouterProvider;
import tusur.jd.taskalaram.base.BasePresenter;
import tusur.jd.taskalaram.tuple.Tuple2;
import tusur.jd.taskalaram.viewmodels.AlarmViewModel;
import tusur.jd.taskalaram.viewmodels.CreateAlarmViewModel;

public class AlarmsHomePresenter extends BasePresenter {

	@NonNull
	private final View mView;
	@NonNull
	private final AlarmsInteractor mAlarmsInteractor;
	@NonNull
	private final NotesInteractor mNotesInteractor;
	@NonNull
	private final UiRouterProvider mUiRouterProvider;
	@NonNull
	private final SimpleDateFormat mFormater;

	public AlarmsHomePresenter(@NonNull final View view,
	                           @NonNull final AlarmsInteractor alarmsInteractor,
	                           @NonNull final NotesInteractor notesInteractor,
	                           @NonNull final UiRouterProvider uiRouterProvider) {
		mView = view;
		mAlarmsInteractor = alarmsInteractor;
		mNotesInteractor = notesInteractor;
		mUiRouterProvider = uiRouterProvider;
		mFormater = new SimpleDateFormat("HH:mm", Locale.getDefault());
	}

	@Override
	public void onViewShown() {
		super.onViewShown();
		addSubscription(
				Observable.combineLatest(
						mAlarmsInteractor.getAlarmsObservable(),
						mNotesInteractor.getNotesObservable(),
						Tuple2::new
				).map(tuple -> mapModels(tuple.getFirst(), tuple.getSecond())), mView::setItems);
	}

	public void onCreateAlarmClicked() {
		mUiRouterProvider.get().openAlarmEditPage();
	}

	public void onAlarmClicked(@NonNull final AlarmViewModel model) {
		mUiRouterProvider.get().openAlarmEditPage(model.getId());
	}

	@NonNull
	private List<ViewModel> mapModels(final List<Alarm> alarms, final List<Note> notes) {
		final List<ViewModel> models = Stream.of(alarms).map(alarm -> {
			Optional<Note> note = Stream.of(notes).filter(it -> it.getId() == alarm.getNoteId()).findFirst();
			return new AlarmViewModel(
					alarm.getId(),
					note.map(Note::getId).orElseGet(() -> -1L),
					formatTime(alarm.getHours(), alarm.getMinutes()),
					note.map(Note::getName).orElseGet(() -> "")
			);
		}).collect(Collectors.toList());
		models.add(new CreateAlarmViewModel());
		return models;
	}

	@NonNull
	private String formatTime(final int hours, final int minutes) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, hours);
		calendar.set(Calendar.MINUTE, minutes);
		return mFormater.format(calendar.getTime());
	}

	interface View extends IBaseView {

		void setItems(@NonNull List<ViewModel> items);
	}

}