package tusur.jd.taskalaram.alarms;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder;

import java.util.List;

import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.base.BaseDiffCallback;
import tusur.jd.taskalaram.base.BaseScreenFragment;
import tusur.jd.taskalaram.viewmodels.AlarmViewModel;
import tusur.jd.taskalaram.viewmodels.CreateAlarmViewModel;

public class AlarmsHomeFragment extends BaseScreenFragment<AlarmsHomePresenter> {

	private ViewHolder mViewHolder;
	private RendererRecyclerViewAdapter mAdapter;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = new AlarmsHomePresenter(
				mViewPresenter,
				mInjector.getAlarmsInteractor(),
				mInjector.getNotesInteractor(),
				getUiRouterProvider()
		);
	}

	@Nullable
	@Override
	public View onCreateScreenView(final LayoutInflater inflater,
	                               @Nullable final ViewGroup container,
	                               @Nullable final Bundle savedInstanceState) {
		mViewHolder = new ViewHolder(inflater.inflate(R.layout.fragment_alarms_home, container, false));
		mAdapter = new RendererRecyclerViewAdapter();
		mAdapter.setDiffCallback(new BaseDiffCallback());

		mAdapter.registerRenderer(new ViewBinder<>(
				R.layout.item_alarm,
				AlarmViewModel.class,
				(model, finder, payloads) -> finder
						.setText(R.id.time, model.getTime())
						.setText(R.id.note_name, model.getNoteName())
						.setOnClickListener(v -> mPresenter.onAlarmClicked(model))
		));

		mAdapter.registerRenderer(new ViewBinder<>(
				R.layout.item_create_alarm,
				CreateAlarmViewModel.class,
				(model, finder, payloads) -> finder.setOnClickListener(v -> mPresenter.onCreateAlarmClicked())
		));

		mViewHolder.recyclerView.setAdapter(mAdapter);
		mViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		mViewHolder.recyclerView.addItemDecoration(mItemDecoration);

		return mViewHolder.rootView;
	}

	@NonNull
	private final RecyclerView.ItemDecoration mItemDecoration = new RecyclerView.ItemDecoration() {

		@Override
		public void getItemOffsets(@NonNull final Rect outRect,
		                           @NonNull final View view,
		                           @NonNull final RecyclerView parent,
		                           @NonNull final RecyclerView.State state) {
			super.getItemOffsets(outRect, view, parent, state);
			final RecyclerView.Adapter adapter = parent.getAdapter();
			final int position = parent.getChildAdapterPosition(view);
			final boolean isFirstPosition = position == 0;
			final boolean isLastPosition = position == adapter.getItemCount() - 1;

			outRect.top = dpToPx(6);
			outRect.bottom = dpToPx(6);
			outRect.left = dpToPx(24);
			outRect.right = dpToPx(24);

			if (isFirstPosition) {
				outRect.top = dpToPx(24);
			}
			if (isLastPosition) {
				outRect.bottom = dpToPx(24);
			}
		}
	};

	@NonNull
	private final AlarmsHomePresenter.View mViewPresenter = new AlarmsHomePresenter.View() {

		@Override
		public void setItems(@NonNull final List<ViewModel> items) {
			mAdapter.setItems(items);
		}

		@NonNull
		@Override
		public Context getViewContext() {
			return getContext();
		}
	};

	private class ViewHolder {

		private final View rootView;
		private final RecyclerView recyclerView;

		public ViewHolder(@NonNull final View view) {
			rootView = view;
			recyclerView = view.findViewById(R.id.recycler_view);
		}
	}

}