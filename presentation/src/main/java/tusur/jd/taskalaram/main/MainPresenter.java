package tusur.jd.taskalaram.main;

import android.support.annotation.NonNull;

import tusur.jd.domain.interactors.AlarmsInteractor;
import tusur.jd.domain.interactors.NotesInteractor;
import tusur.jd.taskalaram.base.BasePresenter;

public class MainPresenter extends BasePresenter {

	@NonNull
	private final View mView;
	@NonNull
	private final NotesInteractor mNotesInteractor;
	@NonNull
	private final AlarmsInteractor mAlarmsInteractor;

	public MainPresenter(@NonNull final View view,
	                     @NonNull final NotesInteractor notesInteractor,
	                     @NonNull final AlarmsInteractor alarmsInteractor) {
		mView = view;
		mNotesInteractor = notesInteractor;
		mAlarmsInteractor = alarmsInteractor;
	}

	@Override
	public void onViewShown() {
		super.onViewShown();

	}

	public void onClearNotesClicked() {
		mNotesInteractor.deleteAll();
	}

	public void onClearAlarmsClicked() {
		mAlarmsInteractor.deleteAll();
	}

	interface View extends IBaseView {

	}

}