package tusur.jd.taskalaram.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.annimon.stream.Optional;

import tusur.jd.data.database.DatabaseGateway;
import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.base.BaseActivity;

import static tusur.jd.data.repository.AlarmsRepository.KEY_FROM_ALARM;

public class MainActivity extends BaseActivity {

	private ViewHolder mViewHolder;
	private MainPresenter mPresenter;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mPresenter = new MainPresenter(mPresenterView, mInjector.getNotesInteractor(), mInjector.getAlarmsInteractor());
		mViewHolder = new ViewHolder();

		setSupportActionBar(mViewHolder.toolbar);

		final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, mViewHolder.drawer, mViewHolder.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		mViewHolder.drawer.addDrawerListener(toggle);
		toggle.syncState();

		mViewHolder.navigationView.setNavigationItemSelectedListener(mNavigationListener);
		mViewHolder.navigationView.setCheckedItem(R.id.nav_alarms);
		mNavigationListener.onNavigationItemSelected(mViewHolder.navigationView.getMenu().getItem(0));
		onHandleIntent(getIntent());
	}

	@Override
	protected void onNewIntent(@NonNull final Intent intent) {
		super.onNewIntent(intent);
		onHandleIntent(intent);
	}

	private void onHandleIntent(@NonNull final Intent intent) {
		Optional.ofNullable(intent.getExtras()).executeIfPresent(extras -> {
			final boolean fromAlarm = extras.getBoolean(KEY_FROM_ALARM, false);
			if (fromAlarm) {
				final long alarmId = extras.getLong(DatabaseGateway.KeyAlarm.KEY_ID, -1L);
				mUiRouterProvider.get().openActiveAlarmPage(alarmId);
			}
		});
	}

	@Override
	public void onBackPressed() {
		if (mViewHolder.drawer.isDrawerOpen(GravityCompat.START)) {
			mViewHolder.drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_clear_notes: {
				mPresenter.onClearNotesClicked();
				return true;
			}
			case R.id.action_clear_alarms: {
				mPresenter.onClearAlarmsClicked();
				return true;
			}
			default: {
				return super.onOptionsItemSelected(item);
			}
		}
	}

	@NonNull
	private final NavigationView.OnNavigationItemSelectedListener mNavigationListener =
			new NavigationView.OnNavigationItemSelectedListener() {
				@Override
				public boolean onNavigationItemSelected(@NonNull final MenuItem menuItem) {
					switch (menuItem.getItemId()) {
						case R.id.nav_notes: {
							mUiRouter.openHomeNotesPage();
							break;
						}
						case R.id.nav_alarms: {
							mUiRouter.openAlarmsHomePage();
							break;
						}
					}
					mViewHolder.drawer.closeDrawer(GravityCompat.START);
					return true;
				}
			};

	private class ViewHolder {

		private final DrawerLayout drawer = findViewById(R.id.drawer_layout);
		private final Toolbar toolbar = findViewById(R.id.toolbar);
		private final NavigationView navigationView = findViewById(R.id.nav_view);
	}

	@NonNull
	private final MainPresenter.View mPresenterView = new MainPresenter.View() {
		@NonNull
		@Override
		public Context getViewContext() {
			return getBaseContext();
		}
	};

}
