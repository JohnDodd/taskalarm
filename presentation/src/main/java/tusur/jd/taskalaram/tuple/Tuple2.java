package tusur.jd.taskalaram.tuple;

import android.support.annotation.NonNull;

public class Tuple2 <T1, T2> {

	@NonNull
	private final T1 mFirst;
	@NonNull
	private final T2 mSecond;

	public Tuple2(@NonNull final T1 first, @NonNull final T2 second) {
		mFirst = first;
		mSecond = second;
	}

	@NonNull
	public T1 getFirst() {
		return mFirst;
	}

	@NonNull
	public T2 getSecond() {
		return mSecond;
	}
}
