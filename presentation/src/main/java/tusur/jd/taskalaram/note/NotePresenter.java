package tusur.jd.taskalaram.note;

import android.support.annotation.NonNull;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;

import java.util.List;

import tusur.jd.domain.interactors.NotesInteractor;
import tusur.jd.domain.interactors.RecordsInteractor;
import tusur.jd.domain.models.Note;
import tusur.jd.domain.models.Record;
import tusur.jd.taskalaram.base.BasePresenter;
import tusur.jd.taskalaram.viewmodels.RecordViewModel;

public class NotePresenter extends BasePresenter {

	@NonNull
	private final View mView;
	private final long mNoteId;
	@NonNull
	private final NotesInteractor mNotesInteractor;
	@NonNull
	private final RecordsInteractor mRecordsInteractor;

	public NotePresenter(@NonNull final View view,
	                     final long noteId,
	                     @NonNull final NotesInteractor notesInteractor,
	                     @NonNull final RecordsInteractor recordsInteractor) {
		mView = view;
		mNoteId = noteId;
		mNotesInteractor = notesInteractor;
		mRecordsInteractor = recordsInteractor;
	}

	@Override
	public void onViewShown() {
		super.onViewShown();
		addSubscription(mNotesInteractor.getNoteObservable(mNoteId).map(Note::getName), mView::showTitle);
		addSubscription(mRecordsInteractor.getRecordsObservable(mNoteId).map(this::mapModels), mView::setItems);
	}

	@NonNull
	private List<ViewModel> mapModels(@NonNull final List<Record> records) {
		return Stream.of(records)
				.filterNot(it -> it.getText().isEmpty())
				.map(record -> new RecordViewModel(record.getId(), record.getText()))
				.collect(Collectors.toList());
	}

	public void onPositiveButtonClicked() {
		mView.close();
	}

	interface View extends IBaseView {

		void setItems(@NonNull List<ViewModel> items);
		void close();
		void showTitle(@NonNull String title);
	}

}