package tusur.jd.taskalaram.note;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder;

import java.util.List;

import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.base.BaseDialogFragment;
import tusur.jd.taskalaram.base.BaseDiffCallback;
import tusur.jd.taskalaram.viewmodels.RecordViewModel;

public class NoteDialogFragment extends BaseDialogFragment<NotePresenter> {

	public static final String KEY_NOTE_ID = "KEY_NOTE_ID";
	private ViewHolder mViewHolder;
	private RendererRecyclerViewAdapter mAdapter;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = new NotePresenter(
				mViewPresenter,
				getArg(KEY_NOTE_ID, -1L),
				mInjector.getNotesInteractor(),
				mInjector.getRecordsInteractor()
		);
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {

		mViewHolder = new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.fragment_note, null, false));
		mAdapter = new RendererRecyclerViewAdapter();
		mAdapter.setDiffCallback(new BaseDiffCallback());

		mAdapter.registerRenderer(new ViewBinder<>(
				R.layout.item_record,
				RecordViewModel.class,
				(model, finder, payloads) -> finder
						.setText(R.id.record_text, model.getText())
		));

		mViewHolder.recyclerView.setAdapter(mAdapter);
		mViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		mViewHolder.recyclerView.addItemDecoration(mItemDecoration);
		return new AlertDialog.Builder(getContext())
				.setTitle("Empty")
				.setPositiveButton(R.string.ok, (dialog, which) -> mPresenter.onPositiveButtonClicked())
				.setView(mViewHolder.rootView)
				.create();
	}

	@NonNull
	private final NotePresenter.View mViewPresenter = new NotePresenter.View() {

		@Override
		public void setItems(@NonNull final List<ViewModel> items) {
			mAdapter.setItems(items);
		}

		@Override
		public void close() {
			dismiss();
		}

		@Override
		public void showTitle(@NonNull final String title) {
			getDialog().setTitle(title);
		}

		@NonNull
		@Override
		public Context getViewContext() {
			return getContext();
		}
	};

	@NonNull
	private final RecyclerView.ItemDecoration mItemDecoration = new RecyclerView.ItemDecoration() {

		@Override
		public void getItemOffsets(@NonNull final Rect outRect,
		                           @NonNull final View view,
		                           @NonNull final RecyclerView parent,
		                           @NonNull final RecyclerView.State state) {
			super.getItemOffsets(outRect, view, parent, state);
			final RecyclerView.Adapter adapter = parent.getAdapter();
			final int position = parent.getChildAdapterPosition(view);
			final boolean isFirstPosition = position == 0;
			final boolean isLastPosition = position == adapter.getItemCount() - 1;

			outRect.top = dpToPx(6);
			outRect.bottom = dpToPx(6);
			outRect.left = dpToPx(24);
			outRect.right = dpToPx(24);

			if (isFirstPosition) {
				outRect.top = dpToPx(24);
			}
			if (isLastPosition) {
				outRect.bottom = dpToPx(24);
			}
		}
	};

	private class ViewHolder {

		private final View rootView;
		private final RecyclerView recyclerView;

		public ViewHolder(@NonNull final View view) {
			rootView = view;
			recyclerView = view.findViewById(R.id.recycler_view);
		}
	}

}