package tusur.jd.taskalaram.alarmedit;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.annimon.stream.Optional;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.List;

import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.base.BaseScreenFragment;
import tusur.jd.taskalaram.viewmodels.NoteViewModel;

public class AlarmEditFragment extends BaseScreenFragment<AlarmEditPresenter> {

	private ViewHolder mViewHolder;
	private Adapter mNotesAdapter;
	public static final String ALARM_ID = "ALARM_ID";

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = new AlarmEditPresenter(
				mViewPresenter,
				mInjector.getNotesInteractor(),
				mInjector.getAlarmsInteractor(),
				Optional.ofNullable(getArguments()).map(it -> it.getLong(ALARM_ID)).orElseGet(() -> -1L)
		);
	}

	@Nullable
	@Override
	public View onCreateScreenView(@NonNull final LayoutInflater inflater,
	                               @Nullable final ViewGroup container,
	                               @Nullable final Bundle savedInstanceState) {
		mViewHolder = new ViewHolder(inflater.inflate(R.layout.fragment_alarm_edit, container, false));

		mNotesAdapter = new Adapter(getContext());
		mViewHolder.notesSpinner.setAdapter(mNotesAdapter);
		mViewHolder.notesSpinner.setOnItemSelectedListener(mOnNoteSelectedListener);
		mViewHolder.timeText.setOnClickListener(v -> mPresenter.onTimeClicked());
		mViewHolder.createAlarmButton.setOnClickListener(v -> mPresenter.onCreateAlarmClicked());
		mViewHolder.editAlarmButton.setOnClickListener(v -> mPresenter.onEditAlarmClicked());

		return mViewHolder.rootView;
	}

	private class Adapter extends ArrayAdapter<NoteViewModel> {

		private Adapter(@NonNull final Context context) {
			super(context, android.R.layout.simple_spinner_item);
		}

		@Override
		public View getDropDownView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
			final ViewHolder viewHolder;
			final View finalView;
			if (convertView == null) {
				finalView = LayoutInflater.from(this.getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);

				viewHolder = new ViewHolder();
				viewHolder.itemView = finalView.findViewById(android.R.id.text1);

				finalView.setTag(viewHolder);
			} else {
				finalView = convertView;
				viewHolder = (ViewHolder) finalView.getTag();
			}

			final NoteViewModel item = getItem(position);
			if (item != null) {
				viewHolder.itemView.setText(item.getName());
			}

			return finalView;
		}

		@NonNull
		public View getView(final int position, View convertView, @NonNull final ViewGroup parent) {
			final ViewHolder viewHolder;
			if (convertView == null) {
				convertView = LayoutInflater.from(this.getContext()).inflate(android.R.layout.simple_spinner_item, parent, false);

				viewHolder = new ViewHolder();
				viewHolder.itemView = convertView.findViewById(android.R.id.text1);

				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			final NoteViewModel item = getItem(position);
			if (item != null) {
				viewHolder.itemView.setText(item.getName());
			}

			return convertView;
		}

		private class ViewHolder {

			private TextView itemView;

		}

	}

	@NonNull
	private final AdapterView.OnItemSelectedListener mOnNoteSelectedListener = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
			Optional.ofNullable(mNotesAdapter.getItem(position)).executeIfPresent(mPresenter::onNoteClicked);
		}

		@Override
		public void onNothingSelected(final AdapterView<?> parent) {
		}
	};


	private TimePickerDialog mTimePickerDialog;
	@NonNull
	private final AlarmEditPresenter.View mViewPresenter = new AlarmEditPresenter.View() {
		@NonNull
		@Override
		public Context getViewContext() {
			return getContext();
		}

		@Override
		public void setNotesItems(@NonNull final List<NoteViewModel> items) {
			mNotesAdapter.clear();
			mNotesAdapter.addAll(items);
			mNotesAdapter.notifyDataSetChanged();
		}

		@Override
		public void close() {
			popBackStack();
		}

		@Override
		public void showTimeView() {
			final Calendar calendar = Calendar.getInstance();
			if (mTimePickerDialog == null) {
				mTimePickerDialog = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
					mPresenter.onTimeChanged(hourOfDay, minute);
				}, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
				mTimePickerDialog.setOnDismissListener(dialog -> mTimePickerDialog = null);
				mTimePickerDialog.show(getActivity().getFragmentManager(), null);
			}
		}

		@Override
		public void setTime(@NonNull final String text) {
			mViewHolder.timeText.setText(text);
		}

		@Override
		public void selectItem(final int index) {
			mViewHolder.notesSpinner.setSelection(index);
		}

		@Override
		public void showEditButton() {
			mViewHolder.editAlarmButton.setVisibility(View.VISIBLE);
			mViewHolder.createAlarmButton.setVisibility(View.GONE);
		}

		@Override
		public void showCreateButton() {
			mViewHolder.editAlarmButton.setVisibility(View.GONE);
			mViewHolder.createAlarmButton.setVisibility(View.VISIBLE);
		}
	};

	private class ViewHolder {

		private final View rootView;
		private final AppCompatSpinner notesSpinner;
		private final AppCompatTextView timeText;
		private final View createAlarmButton;
		private final View editAlarmButton;

		public ViewHolder(@NonNull final View view) {
			rootView = view;
			notesSpinner = view.findViewById(R.id.notes_spinner);
			timeText = view.findViewById(R.id.time_picker);
			createAlarmButton = view.findViewById(R.id.create_alarm_button);
			editAlarmButton = view.findViewById(R.id.edit_alarm_button);
		}
	}

}