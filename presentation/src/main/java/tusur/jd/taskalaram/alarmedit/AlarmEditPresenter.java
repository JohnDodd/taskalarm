package tusur.jd.taskalaram.alarmedit;

import android.support.annotation.NonNull;

import com.annimon.stream.IntPair;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.subjects.BehaviorSubject;
import tusur.jd.domain.interactors.AlarmsInteractor;
import tusur.jd.domain.interactors.NotesInteractor;
import tusur.jd.domain.models.Alarm;
import tusur.jd.domain.models.Note;
import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.base.BasePresenter;
import tusur.jd.taskalaram.tuple.Tuple2;
import tusur.jd.taskalaram.viewmodels.NoteViewModel;

public class AlarmEditPresenter extends BasePresenter {

	@NonNull
	private final View mView;
	@NonNull
	private final NotesInteractor mNotesInteractor;
	@NonNull
	private final AlarmsInteractor mAlarmsInteractor;
	private final long mAlarmId;
	private long mChosenNoteId = -1;
	private BehaviorSubject<Integer> mHourOfDaySubject = BehaviorSubject.createDefault(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
	private BehaviorSubject<Integer> mMinuteSubject = BehaviorSubject.createDefault(Calendar.getInstance().get(Calendar.MINUTE));
	private final SimpleDateFormat mFormater;

	public AlarmEditPresenter(@NonNull final View view,
	                          @NonNull final NotesInteractor notesInteractor,
	                          @NonNull final AlarmsInteractor alarmsInteractor,
	                          final long alarmId) {
		mView = view;
		mNotesInteractor = notesInteractor;
		mAlarmsInteractor = alarmsInteractor;
		mAlarmId = alarmId;
		mFormater = new SimpleDateFormat("HH:mm", Locale.getDefault());
	}

	@Override
	public void onViewShown() {
		super.onViewShown();
		final ConnectableObservable<Optional<Alarm>> alarmObservable = mAlarmsInteractor.getAlarmOptionalObservable(mAlarmId).publish();
		addSubscription(alarmObservable.filter(Optional::isPresent).map(Optional::get), it -> {
			mMinuteSubject.onNext(it.getMinutes());
			mHourOfDaySubject.onNext(it.getHours());
		});

		addSubscription(
				Observable.combineLatest(alarmObservable, mNotesInteractor.getNotesObservable().map(this::mapNotes), Tuple2::new),
				tuple -> {
					mView.setNotesItems(tuple.getSecond());
					tuple.getFirst()
							.map(Alarm::getNoteId)
							.map(it -> Stream.of(tuple.getSecond())
									.indexed()
									.filter(pair -> pair.getSecond().getId() == it)
									.findFirst()
									.map(IntPair::getFirst)
									.orElseGet(() -> 0))
							.executeIfPresent(mView::selectItem);
				}
		);

		addSubscription(
				Observable.combineLatest(mHourOfDaySubject, mMinuteSubject, Tuple2::new)
						.map(it -> {
							final Calendar calendar = Calendar.getInstance();
							calendar.set(Calendar.HOUR_OF_DAY, it.getFirst());
							calendar.set(Calendar.MINUTE, it.getSecond());
							return mFormater.format(calendar.getTime());
						}),
				mView::setTime
		);

		if (mAlarmId >= 0) {
			mView.showEditButton();
		} else {
			mView.showCreateButton();
		}
		alarmObservable.connect();
	}

	@NonNull
	private List<NoteViewModel> mapNotes(@NonNull final List<Note> notes) {
		final List<NoteViewModel> models = Stream.of(notes).map(it -> new NoteViewModel(it.getId(), it.getName(), false)).toList();
		models.add(0, new NoteViewModel(-1, mView.getString(R.string.no_item), false));
		return models;
	}

	public void onNoteClicked(@NonNull final NoteViewModel item) {
		mChosenNoteId = item.getId();
	}

	public void onEditAlarmClicked() {
		mAlarmsInteractor.updateSingleTimeAlarm(mAlarmId, mHourOfDaySubject.getValue(), mMinuteSubject.getValue(), mChosenNoteId);
		mView.close();
	}

	public void onCreateAlarmClicked() {
		mAlarmsInteractor.addSingleTimeAlarm(mHourOfDaySubject.getValue(), mMinuteSubject.getValue(), mChosenNoteId);
		mView.close();
	}

	public void onTimeClicked() {
		mView.showTimeView();
	}

	public void onTimeChanged(final int hourOfDay, final int minute) {
		mHourOfDaySubject.onNext(hourOfDay);
		mMinuteSubject.onNext(minute);
	}

	interface View extends IBaseView {

		void setNotesItems(@NonNull List<NoteViewModel> items);
		void close();
		void showTimeView();
		void setTime(@NonNull String text);
		void selectItem(int index);
		void showEditButton();
		void showCreateButton();
	}

}