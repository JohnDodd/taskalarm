package tusur.jd.taskalaram.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;

import com.annimon.stream.Optional;

import tusur.jd.data.database.DatabaseGateway;
import tusur.jd.data.utils.AppLog;
import tusur.jd.taskalaram.main.MainActivity;

import static tusur.jd.data.repository.AlarmsRepository.KEY_FROM_ALARM;

public class AlarmReceiver extends BroadcastReceiver {

	private static final String TAG = AlarmReceiver.class.getSimpleName();

	@Override
	public void onReceive(final Context context, final Intent intent) {
		AppLog.d(TAG, "onReceive: ");
		Optional.ofNullable(intent.getExtras()).executeIfPresent(extras -> {
			final PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			assert pm != null;
			final PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
			wl.acquire(10 * 60 * 1000L);
			final long alarmId = extras.getLong(DatabaseGateway.KeyAlarm.KEY_ID, -1L);
			AppLog.d(TAG, "onReceive: alarmId " + alarmId);

			final Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
			final Ringtone ringtone = RingtoneManager.getRingtone(context, uri);
			ringtone.play();

			final Intent intentActivity = new Intent(context, MainActivity.class);
			intentActivity.setData(intent.getData());

			final boolean fromAlarm = extras.getBoolean(KEY_FROM_ALARM, false);
			intentActivity.putExtra(KEY_FROM_ALARM, fromAlarm);
			intentActivity.putExtra(DatabaseGateway.KeyAlarm.KEY_ID, alarmId);
			intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intentActivity);
			wl.release();

		});

	}
}
