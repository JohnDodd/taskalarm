package tusur.jd.taskalaram.app;

public interface Provider <T> {

	T get();
}
