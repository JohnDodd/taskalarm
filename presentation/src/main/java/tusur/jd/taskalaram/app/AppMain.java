package tusur.jd.taskalaram.app;

import android.app.Application;
import android.support.annotation.NonNull;

import io.reactivex.plugins.RxJavaPlugins;
import tusur.jd.data.repository.RepositoryProvider;
import tusur.jd.data.utils.AppLog;
import tusur.jd.domain.Injector;
import tusur.jd.domain.repository.IRepositoryProvider;
import tusur.jd.taskalaram.receiver.AlarmReceiver;

public class AppMain extends Application {

	private IRepositoryProvider mRepositoryProvider;

	private Injector mInjector;

	@Override
	public void onCreate() {
		super.onCreate();
		RxJavaPlugins.setErrorHandler(throwable -> AppLog.e(RxJavaPlugins.class.getSimpleName(), "Error handler: : ", throwable));
		mRepositoryProvider = new RepositoryProvider(getApplicationContext(), AlarmReceiver.class);
		mInjector = new Injector(mRepositoryProvider);

	}

	@NonNull
	public Injector getInjector() {
		return mInjector;
	}
}
