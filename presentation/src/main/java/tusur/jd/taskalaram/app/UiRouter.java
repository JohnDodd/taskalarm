package tusur.jd.taskalaram.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.annimon.stream.Optional;

import java.io.Serializable;

import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.alarm.ActiveAlarmFragment;
import tusur.jd.taskalaram.alarmedit.AlarmEditFragment;
import tusur.jd.taskalaram.alarms.AlarmsHomeFragment;
import tusur.jd.taskalaram.base.BaseActivity;
import tusur.jd.taskalaram.base.BaseDialogFragment;
import tusur.jd.taskalaram.base.BaseScreenFragment;
import tusur.jd.taskalaram.editnote.EditNoteFragment;
import tusur.jd.taskalaram.homenotes.HomeNotesFragment;
import tusur.jd.taskalaram.note.NoteDialogFragment;

import static android.content.Intent.ACTION_MAIN;
import static android.content.Intent.CATEGORY_HOME;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static tusur.jd.taskalaram.editnote.EditNoteFragment.KEY_NOTE_ID;

public class UiRouter {

	@NonNull
	private final BaseActivity mContext;
	@NonNull
	private final FragmentManager mFragmentManager;

	public UiRouter(@NonNull final BaseActivity context) {
		mContext = context;
		mFragmentManager = context.getSupportFragmentManager();
	}

	public void openHomeNotesPage() {
		openPage(HomeNotesFragment::new, HomeNotesFragment.class);
	}

	public void openAlarmsHomePage() {
		openPage(AlarmsHomeFragment::new, AlarmsHomeFragment.class);
	}

	public void openAlarmEditPage() {
		openAlarmEditPage(-1L);
	}

	public void openAlarmEditPage(final long id) {
		openPage(
				FragmentBuilder.from(AlarmEditFragment::new)
						.put(AlarmEditFragment.ALARM_ID, id)
						.build(),
				AlarmEditFragment.class
		);
	}

	public void openActiveAlarmPage(final long alarmId) {
		openPage(
				FragmentBuilder.from(ActiveAlarmFragment::new)
						.put(ActiveAlarmFragment.KEY_ALARM_ID, alarmId)
						.build(),
				ActiveAlarmFragment.class
		);
	}

	@NonNull
	public void openNoteDialog(final long noteId) {
		openDialog(
				FragmentBuilder.from(NoteDialogFragment::new)
						.put(NoteDialogFragment.KEY_NOTE_ID, noteId)
						.build(),
				NoteDialogFragment.class
		);
	}

	public void openEditNotePage(final long noteId) {
		final Provider<EditNoteFragment> fragmentProvider = () -> {
			final Bundle bundle = new Bundle();
			bundle.putLong(KEY_NOTE_ID, noteId);
			final EditNoteFragment fragment = new EditNoteFragment();
			fragment.setArguments(bundle);
			return fragment;
		};
		openPage(fragmentProvider, EditNoteFragment.class);
	}

	public void openDesktopPage() {
		mContext.startActivity(new Intent(ACTION_MAIN)
				.addCategory(CATEGORY_HOME)
				.setFlags(FLAG_ACTIVITY_NEW_TASK));
	}

	private <T extends BaseDialogFragment> void openDialog(@NonNull final Provider<T> fragmentProvider,
	                                                       @NonNull final Class<T> fragmentClass) {
		Optional.ofNullable(mFragmentManager.findFragmentByTag(fragmentClass.getName()))
				.executeIfAbsent(() -> fragmentProvider.get().show(mFragmentManager, fragmentClass.getName()));
	}

	private <T extends BaseScreenFragment> void openPage(@NonNull final Provider<T> fragmentProvider,
	                                                     @NonNull final Class<T> fragmentClass) {
		replaceIfNeed(R.id.main_container, fragmentProvider, fragmentClass);
	}

	private <T extends BaseScreenFragment> void replaceIfNeed(@IdRes final int id,
	                                                          @NonNull final Provider<T> fragmentProvider,
	                                                          @NonNull final Class<T> fragmentClass) {
		Optional.ofNullable(mFragmentManager.findFragmentByTag(fragmentClass.getName()))
				.executeIfPresent(fragment -> mFragmentManager.popBackStackImmediate(fragmentClass.getName(), 0))
				.executeIfAbsent(() -> {
					final Fragment fragmentById = mFragmentManager.findFragmentById(id);
					if (fragmentById == null || !fragmentClass.isInstance(fragmentById)) {
						replaceWithTag(id, fragmentProvider.get());
					}
				});

	}

	private <T extends BaseScreenFragment> void replaceWithTag(final int id, @NonNull final T fragment) {
		mFragmentManager.beginTransaction()
				.replace(id, fragment)
				.addToBackStack(fragment.getClass().getName())
				.commitAllowingStateLoss();
	}

	private static class FragmentBuilder <T extends Fragment> {

		@NonNull
		private final Provider<T> mFragmentProvider;
		@NonNull
		private final Bundle mBundle;

		private FragmentBuilder(@NonNull final Provider<T> fragmentProvider) {
			this.mFragmentProvider = fragmentProvider;
			mBundle = new Bundle();
		}

		public static <F extends Fragment> FragmentBuilder<F> from(@NonNull final Provider<F> fragmentProvider) {
			return new FragmentBuilder<>(fragmentProvider);
		}

		@NonNull
		public <S extends Serializable> FragmentBuilder<T> put(@NonNull final String key, @NonNull final S value) {
			mBundle.putSerializable(key, value);
			return this;
		}

		@NonNull
		public Provider<T> build() {
			return () -> {
				final T fragment = mFragmentProvider.get();
				fragment.setArguments(mBundle);
				return fragment;
			};
		}

	}

}
