package tusur.jd.taskalaram.app;

import android.support.annotation.NonNull;

import tusur.jd.taskalaram.base.BaseActivity;

public class UiRouterProvider implements Provider<UiRouter> {

	@NonNull
	private final BaseActivity mContext;

	public UiRouterProvider(@NonNull final BaseActivity context) {
		mContext = context;
	}

	@Override
	public UiRouter get() {
		return mContext.getUiRouter();
	}
}
