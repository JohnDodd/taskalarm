package tusur.jd.taskalaram.viewmodels;

import android.support.annotation.NonNull;

import tusur.jd.taskalaram.base.BaseViewModel;

public class NoteViewModel implements BaseViewModel {

	private final long mId;
	@NonNull
	private final String mName;
	private final boolean mChecked;

	public NoteViewModel(final long id, @NonNull final String name, final boolean checked) {
		mId = id;
		mName = name;
		mChecked = checked;
	}

	@NonNull
	public String getName() {
		return mName;
	}

	public boolean isChecked() {
		return mChecked;
	}

	@Override
	public long getId() {
		return mId;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final NoteViewModel that = (NoteViewModel) o;

		if (mId != that.mId) {
			return false;
		}
		if (mChecked != that.mChecked) {
			return false;
		}
		return mName.equals(that.mName);
	}

	@Override
	public int hashCode() {
		int result = (int) (mId ^ (mId >>> 32));
		result = 31 * result + mName.hashCode();
		result = 31 * result + (mChecked ? 1 : 0);
		return result;
	}
}
