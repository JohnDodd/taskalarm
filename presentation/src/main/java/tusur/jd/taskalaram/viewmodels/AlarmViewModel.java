package tusur.jd.taskalaram.viewmodels;

import android.support.annotation.NonNull;

import tusur.jd.taskalaram.base.BaseViewModel;

public class AlarmViewModel implements BaseViewModel {

	private final long mId;
	private final long mNoteId;
	@NonNull
	private final String mTime;
	private final String mNoteName;

	public AlarmViewModel(final long id, final long noteId, @NonNull final String time, final String noteName) {
		mId = id;
		mNoteId = noteId;
		mTime = time;
		mNoteName = noteName;
	}

	public boolean hasAttachedNote() {
		return mNoteId >= 0;
	}

	@NonNull
	public String getTime() {
		return mTime;
	}

	public String getNoteName() {
		return mNoteName;
	}

	@Override
	public long getId() {
		return mId;
	}

	public long getNoteId() {
		return mNoteId;
	}
}
