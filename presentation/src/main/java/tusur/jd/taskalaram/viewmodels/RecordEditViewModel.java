package tusur.jd.taskalaram.viewmodels;

import android.support.annotation.NonNull;

import tusur.jd.taskalaram.base.BaseViewModel;

public class RecordEditViewModel implements BaseViewModel {

	private final long mId;
	@NonNull
	private final String mText;

	public RecordEditViewModel(final long id, @NonNull final String text) {
		mId = id;
		mText = text;
	}

	@NonNull
	public String getText() {
		return mText;
	}

	@Override
	public long getId() {
		return mId;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final RecordEditViewModel that = (RecordEditViewModel) o;

		if (mId != that.mId) {
			return false;
		}
		return mText.equals(that.mText);
	}

	@Override
	public int hashCode() {
		int result = (int) (mId ^ (mId >>> 32));
		result = 31 * result + mText.hashCode();
		return result;
	}
}
