package tusur.jd.taskalaram.view;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public abstract class OnSwipeTouchListener implements View.OnTouchListener {

	private final GestureListener mGestureListener = new GestureListener();
	private final GestureDetector mGestureDetector = new GestureDetector(mGestureListener);

	private final int mSwipeThreshold;

	protected OnSwipeTouchListener(final int swipeThreshold) {
		mSwipeThreshold = swipeThreshold;
	}

	@Override
	public boolean onTouch(final View v, final MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_UP) {
			onUp();
		}
		return mGestureDetector.onTouchEvent(event);
	}


	private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onDown(final MotionEvent e) {
			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onFling(final MotionEvent e1, final MotionEvent e2, final float velocityX, final float velocityY) {
//			final float diffY = e2.getY() - e1.getY();
//			if (-diffY > mSwipeThreshold) {
//				onOverSwipe();
//			}
			return false;
		}

		@Override
		public boolean onScroll(final MotionEvent e1, final MotionEvent e2, final float distanceX, final float distanceY) {
			if (Math.abs(distanceY) > Math.abs(distanceX)) {
				onSwipeVertical(distanceY);
			}
			return false;
		}
	}


	public void onUp() {

	}

	public void onOverSwipe() {

	}

	abstract public void onSwipeVertical(final float diffY);

}
