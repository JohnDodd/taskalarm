package tusur.jd.taskalaram.base;

import android.support.annotation.NonNull;

import com.github.vivchar.rendererrecyclerviewadapter.DiffCallback;

public class BaseDiffCallback extends DiffCallback<BaseViewModel> {

	@Override
	public boolean areItemsTheSame(@NonNull final BaseViewModel oldItem, @NonNull final BaseViewModel newItem) {
		return oldItem.getId() == newItem.getId();
	}

	@Override
	public boolean areContentsTheSame(@NonNull final BaseViewModel oldItem, @NonNull final BaseViewModel newItem) {
		return oldItem.equals(newItem);
	}
}
