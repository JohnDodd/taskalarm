package tusur.jd.taskalaram.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;

import com.annimon.stream.Optional;

import java.io.Serializable;

import tusur.jd.domain.Injector;
import tusur.jd.taskalaram.app.UiRouterProvider;

public class BaseFragment <T extends BasePresenter> extends Fragment {

	@Nullable
	protected Listener mListener;
	protected T mPresenter;
	protected Injector mInjector;

	@Override
	public void onAttach(final Context context) {
		super.onAttach(context);
		mListener = (Listener) context;
		mInjector = mListener.getInjector();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override
	public void onStart() {
		super.onStart();
		mPresenter.onViewShown();
	}

	@Override
	public void onStop() {
		super.onStop();
		mPresenter.onViewHidden();
	}

	protected int dpToPx(final int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, getResources().getDisplayMetrics());
	}

	protected UiRouterProvider getUiRouterProvider() {
		return mListener.getUiRouterProvider();
	}

	protected <S extends Serializable> S getArg(@NonNull final String key, @NonNull final S defaultValue) {
		return Optional.ofNullable(getArguments()).map(it -> (S) it.getSerializable(key)).orElseGet(() -> defaultValue);
	}

	interface Listener {

		@NonNull
		Injector getInjector();
		@NonNull
		UiRouterProvider getUiRouterProvider();
	}
}
