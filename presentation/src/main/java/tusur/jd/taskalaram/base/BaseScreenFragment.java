package tusur.jd.taskalaram.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.Optional;

public abstract class BaseScreenFragment <T extends BasePresenter> extends BaseFragment<T> {

	@Nullable
	@Override
	public View onCreateView(@NonNull final LayoutInflater inflater,
	                         @Nullable final ViewGroup container,
	                         @Nullable final Bundle savedInstanceState) {
		return onCreateScreenView(inflater, container, savedInstanceState);
	}

	@Nullable
	public abstract View onCreateScreenView(@NonNull final LayoutInflater inflater,
	                                        @Nullable ViewGroup container,
	                                        @Nullable Bundle savedInstanceState);

	protected void popBackStack() {
		Optional.ofNullable(getFragmentManager()).executeIfPresent(FragmentManager::popBackStack);
	}

	@Override
	public void onStart() {
		super.onStart();
		resetToolbar();
	}

	private void resetToolbar() {
		showToolbar(true);
	}

	protected void showToolbar(final boolean show) {
		Optional.ofNullable(getActivity()).map(Activity::getActionBar).executeIfPresent(actionBar -> {
			if (show) {
				actionBar.show();
			} else {
				actionBar.hide();
			}
		});
	}
}
