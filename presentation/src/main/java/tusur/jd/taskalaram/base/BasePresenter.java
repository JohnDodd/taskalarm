package tusur.jd.taskalaram.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import tusur.jd.data.utils.AppLog;

public abstract class BasePresenter {

	@NonNull
	private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

	public void onViewShown() {

	}

	public void onViewHidden() {
		mCompositeDisposable.clear();
	}

	protected <T> void addSubscription(@NonNull final Observable<T> observable, @NonNull final Consumer<T> consumer) {
		addSubscription(observable, consumer, t -> AppLog.e(getClass().getSimpleName(), "onError: ", t));
	}

	protected <T> void addSubscription(@NonNull final Observable<T> observable,
	                                   @NonNull final Consumer<T> consumer,
	                                   @NonNull final Consumer<Throwable> errorConsumer) {
		mCompositeDisposable.add(observable
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(consumer, errorConsumer));
	}

	public interface IBaseView {

		Context getViewContext();

		default String getString(@StringRes final int resId) {
			return getViewContext().getString(resId);
		}
	}

}
