package tusur.jd.taskalaram.base;

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;

public interface BaseViewModel extends ViewModel {

	long getId();
}
