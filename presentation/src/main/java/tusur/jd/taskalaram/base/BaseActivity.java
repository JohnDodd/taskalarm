package tusur.jd.taskalaram.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.annimon.stream.Optional;

import tusur.jd.domain.Injector;
import tusur.jd.taskalaram.app.AppMain;
import tusur.jd.taskalaram.app.UiRouter;
import tusur.jd.taskalaram.app.UiRouterProvider;

import static android.content.Intent.ACTION_MAIN;
import static android.content.Intent.CATEGORY_HOME;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public abstract class BaseActivity extends AppCompatActivity implements BaseFragment.Listener {

	protected Injector mInjector;
	protected UiRouter mUiRouter;
	protected UiRouterProvider mUiRouterProvider;

	@Override
	protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mInjector = ((AppMain) getApplication()).getInjector();
		mUiRouter = new UiRouter(this);
		mUiRouterProvider = new UiRouterProvider(this);
	}

	public UiRouter getUiRouter() {
		return mUiRouter;
	}

	@NonNull
	@Override
	public Injector getInjector() {
		return mInjector;
	}

	@NonNull
	@Override
	public UiRouterProvider getUiRouterProvider() {
		return mUiRouterProvider;
	}

	@Override
	public void onBackPressed() {
		Optional.ofNullable(getSupportFragmentManager()).executeIfPresent(fragmentManager -> {
			final int entryCount = fragmentManager.getBackStackEntryCount();
			if (entryCount <= 1) {
				getUiRouterProvider().get().openDesktopPage();
			} else {
				fragmentManager.popBackStack();
			}
		});
	}
}
