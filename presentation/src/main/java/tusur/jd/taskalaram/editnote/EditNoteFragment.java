package tusur.jd.taskalaram.editnote;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.Optional;
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder;

import java.util.List;

import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.base.BaseDiffCallback;
import tusur.jd.taskalaram.base.BaseScreenFragment;
import tusur.jd.taskalaram.viewmodels.CreateRecordViewModel;
import tusur.jd.taskalaram.viewmodels.RecordEditViewModel;

public class EditNoteFragment extends BaseScreenFragment<EditNotePresenter> {

	private ViewHolder mViewHolder;
	public static final String KEY_NOTE_ID = "KEY_NOTE_ID";
	private RendererRecyclerViewAdapter mAdapter;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = new EditNotePresenter(
				mPresenterView,
				Optional.ofNullable(getArguments()).map(it -> it.getLong(KEY_NOTE_ID)).orElseGet(() -> -1L),
				mInjector.getNotesInteractor(),
				mInjector.getRecordsInteractor()
		);
	}

	@Nullable
	@Override
	public View onCreateScreenView(@NonNull final LayoutInflater inflater,
	                               @Nullable final ViewGroup container,
	                               @Nullable final Bundle savedInstanceState) {
		mViewHolder = new ViewHolder(inflater.inflate(R.layout.fragment_edit_note, container, false));

		mAdapter = new RendererRecyclerViewAdapter();
		mAdapter.setDiffCallback(new BaseDiffCallback());
		mAdapter.registerRenderer(new ViewBinder<>(
				R.layout.item_edit_record,
				RecordEditViewModel.class,
				(model, finder, payloads) -> finder
						.setOnClickListener(v -> mPresenter.onRecordClicked(model))
						.<AppCompatEditText>find(R.id.record_text, view ->
								view.setOnFocusChangeListener((View.OnFocusChangeListener) (v, focused) ->
										mPresenter.onRecordFocusChanged(model, focused)
								)
						)
		));

		mAdapter.registerRenderer(new ViewBinder<>(
				R.layout.item_create_record,
				CreateRecordViewModel.class,
				(model, finder, payloads) -> finder
						.<AppCompatEditText>find(R.id.record_text, view ->
								view.setOnFocusChangeListener((View.OnFocusChangeListener) (v, focused) ->
										mPresenter.onCreateRecordFocusChanged(view.getText().toString(), focused)
								)
						)
		));
		mViewHolder.recyclerView.setAdapter(mAdapter);
		mViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		mViewHolder.recyclerView.addItemDecoration(mItemDecoration);
		mViewHolder.fab.setOnClickListener(v -> mPresenter.onFabClicked());

		return mViewHolder.rootView;
	}

	@NonNull
	private final EditNotePresenter.View mPresenterView = new EditNotePresenter.View() {
		@Override
		public void setItems(@NonNull final List<ViewModel> models) {
			mAdapter.setItems(models);
		}

		@Override
		public void showFabButton(final boolean show) {
			if (show) {
				mViewHolder.fab.show();
			} else {
				mViewHolder.fab.hide();
			}
		}


		@Override
		public void showNoteNameHint(@NonNull final String hint) {
			mViewHolder.noteName.setHint(hint);
		}


		@Override
		public void showNoteName(@NonNull final String name) {
			mViewHolder.noteName.setText(name);
		}

		@NonNull
		@Override
		public String getNameText() {
			return Optional.ofNullable(mViewHolder.noteName.getText()).map(CharSequence::toString).orElseGet(() -> "");
		}

		@NonNull
		@Override
		public String getNameHint() {
			return mViewHolder.noteName.getHint().toString();
		}

		@NonNull
		@Override
		public Context getViewContext() {
			return getContext();
		}
	};

	@NonNull
	private final RecyclerView.ItemDecoration mItemDecoration = new RecyclerView.ItemDecoration() {

		@Override
		public void getItemOffsets(@NonNull final Rect outRect,
		                           @NonNull final View view,
		                           @NonNull final RecyclerView parent,
		                           @NonNull final RecyclerView.State state) {
			super.getItemOffsets(outRect, view, parent, state);
			final RecyclerView.Adapter adapter = parent.getAdapter();
			final int position = parent.getChildAdapterPosition(view);
			final boolean isFirstPosition = position == 0;
			final boolean isLastPosition = position == adapter.getItemCount() - 1;

			outRect.top = dpToPx(12);
			outRect.bottom = dpToPx(12);
			outRect.left = dpToPx(24);
			outRect.right = dpToPx(24);

			if (isFirstPosition) {
				outRect.top = dpToPx(24);
			}
			if (isLastPosition) {
				outRect.bottom = dpToPx(24);
			}
		}
	};


	private class ViewHolder {

		private final View rootView;
		private final RecyclerView recyclerView;
		public final AppCompatEditText noteName;
		public final FloatingActionButton fab;

		public ViewHolder(@NonNull final View view) {
			rootView = view;
			recyclerView = view.findViewById(R.id.recycler_view);
			noteName = view.findViewById(R.id.note_name);
			fab = view.findViewById(R.id.fab);
		}
	}

}