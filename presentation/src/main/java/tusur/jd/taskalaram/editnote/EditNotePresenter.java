package tusur.jd.taskalaram.editnote;

import android.support.annotation.NonNull;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import tusur.jd.domain.interactors.NotesInteractor;
import tusur.jd.domain.interactors.RecordsInteractor;
import tusur.jd.domain.models.Note;
import tusur.jd.domain.models.Record;
import tusur.jd.taskalaram.base.BasePresenter;
import tusur.jd.taskalaram.tuple.Tuple2;
import tusur.jd.taskalaram.viewmodels.CreateRecordViewModel;
import tusur.jd.taskalaram.viewmodels.RecordEditViewModel;

import static io.reactivex.subjects.BehaviorSubject.createDefault;
import static io.reactivex.subjects.PublishSubject.create;

public class EditNotePresenter extends BasePresenter {

	@NonNull
	private final View mView;
	private final long mNoteId;
	@NonNull
	private final NotesInteractor mNotesInteractor;
	@NonNull
	private final RecordsInteractor mRecordsInteractor;
	@NonNull
	private BehaviorSubject<List<Record>> mRecordsSubject = createDefault(new ArrayList<>());
	@NonNull
	private PublishSubject<Boolean> mOnFabClickedSubject = create();

	public EditNotePresenter(@NonNull final View view,
	                         final long noteId,
	                         @NonNull final NotesInteractor notesInteractor,
	                         @NonNull final RecordsInteractor recordsInteractor) {
		mView = view;
		mNoteId = noteId;
		mNotesInteractor = notesInteractor;
		mRecordsInteractor = recordsInteractor;
	}

	@Override
	public void onViewShown() {
		super.onViewShown();
		final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
		addSubscription(mNotesInteractor.getNoteObservable(mNoteId).map(Note::getTimeMillis).map(format::format), mView::showNoteNameHint);
		addSubscription(mNotesInteractor.getNoteObservable(mNoteId).map(Note::getName), mView::showNoteName);
		addSubscription(mRecordsInteractor.getRecordsObservable(mNoteId), mRecordsSubject::onNext);

		final Observable<List<ViewModel>> viewModelsObservable = mRecordsSubject.map(it -> mapModels(it, it.isEmpty()));

		final Observable<Boolean> isLastCreateRecordObservable = viewModelsObservable.map(it -> Stream.of(it)
				.findLast()
				.map(CreateRecordViewModel.class::isInstance)
				.orElseGet(() -> false));

		addSubscription(
				Observable.combineLatest(
						mRecordsSubject,
						isLastCreateRecordObservable.map(it -> !it).flatMap(it -> {
							if (it) {
								return Observable.just(true);
							} else {
								return mOnFabClickedSubject.hide();
							}
						}),
						Tuple2::new
				).map(it -> mapModels(it.getFirst(), it.getSecond() || it.getFirst().isEmpty())), it -> {

				});

		addSubscription(isLastCreateRecordObservable, mView::showFabButton);


		addSubscription(viewModelsObservable, mView::setItems);

	}

	@NonNull
	private List<ViewModel> mapModels(final List<Record> records, final boolean addCreateRecord) {
		final List<ViewModel> models = Stream.of(records)
				.map(it -> new RecordEditViewModel(it.getId(), it.getText()))
				.collect(Collectors.toList());
		if (addCreateRecord) {
			models.add(new CreateRecordViewModel());
		}

		return models;
	}

	@Override
	public void onViewHidden() {
		super.onViewHidden();
		final String name = mView.getNameText();
		mNotesInteractor.update(mNoteId, name.isEmpty() ? mView.getNameHint() : name);

	}

	public void onRecordClicked(@NonNull final RecordEditViewModel model) {

	}

	public void onRecordFocusChanged(@NonNull final RecordEditViewModel model, final boolean focused) {
		if (!focused) {
			mRecordsInteractor.update(model.getText(), model.getId(), mNoteId);
		}
	}

	public void onCreateRecordFocusChanged(@NonNull final String text, final boolean focused) {
		mRecordsInteractor.add(text, mNoteId);
	}

	public void onFabClicked() {
		mOnFabClickedSubject.onNext(true);
	}

	interface View extends IBaseView {

		void setItems(@NonNull List<ViewModel> models);
		void showNoteNameHint(@NonNull String hint);
		void showFabButton(boolean show);
		void showNoteName(@NonNull String name);
		@NonNull
		String getNameText();
		@NonNull
		String getNameHint();
	}

}