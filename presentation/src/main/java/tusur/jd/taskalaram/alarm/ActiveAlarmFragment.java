package tusur.jd.taskalaram.alarm;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.base.BaseScreenFragment;
import tusur.jd.taskalaram.view.OnSwipeTouchListener;

public class ActiveAlarmFragment extends BaseScreenFragment<ActiveAlarmPresenter> {

	public static final String KEY_ALARM_ID = "KEY_ALARM_ID";
	private static final int SWIPE_THRESHOLD = 400;

	private ViewHolder mViewHolder;
	private MediaPlayer mMediaPlayer;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		mMediaPlayer = MediaPlayer.create(getContext(), alarmUri);
		mMediaPlayer.start();

		mPresenter = new ActiveAlarmPresenter(
				mViewPresenter,
				mInjector.getAlarmsInteractor(),
				mInjector.getNotesInteractor(),
				mInjector.getRecordsInteractor(),
				getUiRouterProvider(),
				getArg(KEY_ALARM_ID, -1L)
		);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mMediaPlayer.stop();
	}

	@Override
	public void onStart() {
		super.onStart();
		showToolbar(false);
	}

	@Nullable
	@Override
	public View onCreateScreenView(@NonNull final LayoutInflater inflater,
	                               @Nullable final ViewGroup container,
	                               @Nullable final Bundle savedInstanceState) {
		mViewHolder = new ViewHolder(inflater.inflate(R.layout.fragment_active_alarm, container, false));
		mViewHolder.snooze.setOnClickListener(v -> mPresenter.onSnoozeClicked());
		mViewHolder.rootView.setOnTouchListener(mOnSwipeListener);
		return mViewHolder.rootView;
	}

	@NonNull
	private final OnSwipeTouchListener mOnSwipeListener = new OnSwipeTouchListener(SWIPE_THRESHOLD) {
		@Override
		public void onSwipeVertical(final float diffY) {
			final float translationY = mViewHolder.wrapper.getTranslationY();
			final float deltaY = translationY - diffY;
			if (translationY <= 0 && deltaY <= 0) {
				mViewHolder.wrapper.setTranslationY(deltaY);
				final float abs = Math.abs(mViewHolder.wrapper.getTranslationY());
				final int thresholdFinal = SWIPE_THRESHOLD * 4;
				final float alpha = abs >= thresholdFinal ? 0f : (thresholdFinal - abs) / thresholdFinal;
				mViewHolder.wrapper.setAlpha(alpha);
			}
		}

		@Override
		public void onUp() {
			final float translationY = mViewHolder.wrapper.getTranslationY();
			if (Math.abs(translationY) > SWIPE_THRESHOLD) {
				onOverSwipe();
			} else {
				mViewHolder.wrapper.setTranslationY(0);
				mViewHolder.wrapper.setAlpha(1f);
			}
		}

		@Override
		public void onOverSwipe() {
			popBackStack();
			getUiRouterProvider().get().openDesktopPage();
		}
	};


	@NonNull
	private final ActiveAlarmPresenter.View mViewPresenter = new ActiveAlarmPresenter.View() {
		@Override
		public void showNotePage(final long noteId) {
			getUiRouterProvider().get().openNoteDialog(noteId);
		}

		@Override
		public void setTime(@NonNull final String time) {
			mViewHolder.time.setText(time);
		}

		@NonNull
		@Override
		public Context getViewContext() {
			return getContext();
		}
	};

	private class ViewHolder {

		private final View rootView;
		private final View wrapper;
		private final AppCompatTextView time;
		private final View snooze;

		public ViewHolder(@NonNull final View view) {
			rootView = view;
			wrapper = view.findViewById(R.id.wrapper);
			time = view.findViewById(R.id.time);
			snooze = view.findViewById(R.id.snooze);
		}
	}

}