package tusur.jd.taskalaram.alarm;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import io.reactivex.observables.ConnectableObservable;
import tusur.jd.data.utils.AppLog;
import tusur.jd.domain.interactors.AlarmsInteractor;
import tusur.jd.domain.interactors.NotesInteractor;
import tusur.jd.domain.interactors.RecordsInteractor;
import tusur.jd.domain.models.Alarm;
import tusur.jd.taskalaram.app.UiRouterProvider;
import tusur.jd.taskalaram.base.BasePresenter;

public class ActiveAlarmPresenter extends BasePresenter {

	@NonNull
	private final View mView;
	@NonNull
	private final AlarmsInteractor mAlarmsInteractor;
	@NonNull
	private final NotesInteractor mNotesInteractor;
	@NonNull
	private final RecordsInteractor mRecordsInteractor;
	@NonNull
	private final UiRouterProvider mUiRouterProvider;
	private final long mAlarmId;
	private static final String TAG = ActiveAlarmPresenter.class.getSimpleName();
	private final SimpleDateFormat mFormater;

	public ActiveAlarmPresenter(@NonNull final View view,
	                            @NonNull final AlarmsInteractor alarmsInteractor,
	                            @NonNull final NotesInteractor notesInteractor,
	                            @NonNull final RecordsInteractor recordsInteractor,
	                            @NonNull final UiRouterProvider uiRouterProvider,
	                            final long alarmId) {
		mView = view;
		mAlarmsInteractor = alarmsInteractor;
		mNotesInteractor = notesInteractor;
		mRecordsInteractor = recordsInteractor;
		mUiRouterProvider = uiRouterProvider;
		mAlarmId = alarmId;
		mFormater = new SimpleDateFormat("HH:mm", Locale.getDefault());
	}

	@Override
	public void onViewShown() {
		super.onViewShown();
		final ConnectableObservable<Alarm> alarmObservable = mAlarmsInteractor.getAlarmObservable(mAlarmId).publish();
		addSubscription(alarmObservable.map(it -> formatTime(it.getHours(), it.getMinutes())), mView::setTime);
		addSubscription(alarmObservable.map(Alarm::getNoteId), mView::showNotePage);
		alarmObservable.connect();
	}


	@NonNull
	private String formatTime(final int hours, final int minutes) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, hours);
		calendar.set(Calendar.MINUTE, minutes);
		return mFormater.format(calendar.getTime());
	}

	public void onSnoozeClicked() {
		AppLog.d(TAG, "onSnoozeClicked: ");
	}

	interface View extends IBaseView {

		void showNotePage(long noteId);
		void setTime(@NonNull String time);
	}

}