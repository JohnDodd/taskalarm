package tusur.jd.taskalaram.homenotes;

import android.support.annotation.NonNull;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;

import java.util.List;

import tusur.jd.domain.interactors.NotesInteractor;
import tusur.jd.domain.models.Note;
import tusur.jd.taskalaram.app.UiRouterProvider;
import tusur.jd.taskalaram.base.BasePresenter;
import tusur.jd.taskalaram.viewmodels.NoteViewModel;

public class HomeNotesPresenter extends BasePresenter {

	@NonNull
	private final View mView;
	@NonNull
	private final NotesInteractor mNotesRepository;
	@NonNull
	private final UiRouterProvider mUiRouterProvider;

	public HomeNotesPresenter(@NonNull final View view,
	                          @NonNull final NotesInteractor notesRepository,
	                          @NonNull final UiRouterProvider uiRouterProvider) {
		mView = view;
		mNotesRepository = notesRepository;
		mUiRouterProvider = uiRouterProvider;
	}

	@Override
	public void onViewShown() {
		super.onViewShown();
		addSubscription(mNotesRepository.getNotesObservable().map(this::mapModels), mView::setItems);
	}

	@NonNull
	private List<ViewModel> mapModels(@NonNull final List<Note> notes) {
		return Stream.of(notes).map(it -> new NoteViewModel(it.getId(), it.getName(), false)).collect(Collectors.toList());
	}

	public void onNoteClicked(@NonNull final NoteViewModel model) {
		mUiRouterProvider.get().openEditNotePage(model.getId());
	}

	public void onFabClicked() {
		addSubscription(mNotesRepository.insert(""), noteId -> mUiRouterProvider.get().openEditNotePage(noteId));
	}

	public void onDeleteIconClicked(@NonNull final NoteViewModel model) {
		mNotesRepository.delete(model.getId());
	}

	interface View extends IBaseView {

		void setItems(@NonNull List<ViewModel> items);
	}

}