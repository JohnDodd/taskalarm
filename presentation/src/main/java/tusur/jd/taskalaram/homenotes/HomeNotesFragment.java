package tusur.jd.taskalaram.homenotes;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter;
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel;
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder;

import java.util.List;

import tusur.jd.taskalaram.R;
import tusur.jd.taskalaram.base.BaseDiffCallback;
import tusur.jd.taskalaram.base.BaseScreenFragment;
import tusur.jd.taskalaram.viewmodels.NoteViewModel;

public class HomeNotesFragment extends BaseScreenFragment<HomeNotesPresenter> {

	private ViewHolder mViewHolder;
	private RendererRecyclerViewAdapter mAdapter;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = new HomeNotesPresenter(mPresenterView, mInjector.getNotesInteractor(), getUiRouterProvider());
	}

	@Nullable
	@Override
	public View onCreateScreenView(@NonNull final LayoutInflater inflater,
	                               @Nullable final ViewGroup container,
	                               @Nullable final Bundle savedInstanceState) {
		mViewHolder = new ViewHolder(inflater.inflate(R.layout.fragment_home_notes, container, false));
		mAdapter = new RendererRecyclerViewAdapter();
		mAdapter.setDiffCallback(new BaseDiffCallback());
		mAdapter.registerRenderer(new ViewBinder<>(
				R.layout.item_note,
				NoteViewModel.class,
				(model, finder, payloads) -> finder.setOnClickListener(v -> mPresenter.onNoteClicked(model))
						.setText(R.id.note_name, model.getName())
						.setOnClickListener(R.id.delete_icon, v -> mPresenter.onDeleteIconClicked(model))
		));
		mViewHolder.recyclerView.setAdapter(mAdapter);
		mViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		mViewHolder.recyclerView.addItemDecoration(mItemDecoration);

		mViewHolder.fab.setOnClickListener(v -> mPresenter.onFabClicked());
		return mViewHolder.rootView;
	}

	@NonNull
	private final RecyclerView.ItemDecoration mItemDecoration = new RecyclerView.ItemDecoration() {

		@Override
		public void getItemOffsets(@NonNull final Rect outRect,
		                           @NonNull final View view,
		                           @NonNull final RecyclerView parent,
		                           @NonNull final RecyclerView.State state) {
			super.getItemOffsets(outRect, view, parent, state);
			final RecyclerView.Adapter adapter = parent.getAdapter();
			final int position = parent.getChildAdapterPosition(view);
			final boolean isFirstPosition = position == 0;
			final boolean isLastPosition = position == adapter.getItemCount() - 1;

			outRect.top = dpToPx(6);
			outRect.bottom = dpToPx(6);
			outRect.left = dpToPx(24 * 2);
			outRect.right = dpToPx(24 * 2);

			if (isFirstPosition) {
				outRect.top = dpToPx(24);
			}
			if (isLastPosition) {
				outRect.bottom = dpToPx(24);
			}
		}
	};


	@NonNull
	private final HomeNotesPresenter.View mPresenterView = new HomeNotesPresenter.View() {

		@Override
		public void setItems(@NonNull final List<ViewModel> items) {
			mAdapter.setItems(items);
		}

		@NonNull
		@Override
		public Context getViewContext() {
			return getContext();
		}
	};

	private class ViewHolder {

		private final View rootView;
		private final RecyclerView recyclerView;
		private final FloatingActionButton fab;

		public ViewHolder(@NonNull final View view) {
			rootView = view;
			recyclerView = view.findViewById(R.id.recycler_view);
			fab = view.findViewById(R.id.fab);
		}
	}

}